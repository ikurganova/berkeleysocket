
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 
#include <iostream> 
#include <string> 
#include <thread> 
#include <chrono> 
#include <memory>   
#include "config.h"
#include <utility>
#include <netdb.h>
  
  
class Client
{
protected:
    int sock; 
    char buffer[config::MAX_SIZE_MSG]; 
    struct sockaddr_in servaddr; 
public:
    enum ERROR
    {
        AMOUNT_OF_ARGUMENTS = 0,
        IP_ADDRESS,
        PORT,
        TYPE_PROTOCOL,
        SOCKET_CREATE,
        CONNECT,
        NO_ANSWER,
        AMOUNT_ERRORS
    };
    static std::array<std::string, Client::ERROR::AMOUNT_ERRORS> decryptionEr; 
    static void printEr(ERROR e)
    {
        std::cout << decryptionEr[e] << std::endl;
    }
    Client(std::string addr, int port)
    {
        memset(&servaddr, 0, sizeof(struct sockaddr_in));
        servaddr.sin_family = AF_INET; 

        if ((port > 0) && (port <= 0xffff)) servaddr.sin_port = htons(port); 
        else throw ERROR::PORT;

        servaddr.sin_addr.s_addr = inet_addr(addr.c_str()); 
        if (( in_addr_t)(-1) ==  servaddr.sin_addr.s_addr)  throw ERROR::IP_ADDRESS;

        sock = -1;
        //std::cout << "Client constr" << std::endl;
    }

    virtual void handleConnect() = 0;
    virtual ~Client(){};

protected:
    bool isStop(std::string s)
    {
        if ("stop" == s) return true;
        else return false;
    }
    std::string getMsg()
    {
        std::string s;
        std::cout << "input  message"<< std::endl;
        std::cin >> s;
        return s;
    }
};

std::array<std::string, Client::ERROR::AMOUNT_ERRORS> Client::decryptionEr = 
{
    "Incorrect amount of arguments",
    "Incorrect IP address",
    "Incorrect port number",
    "Incorrect protocol type",
    "Socket creation failed",
    "Connect failed",
    "Server is not responding"
} ;

class TcpClient : public Client 
{

   
public:
    TcpClient(std::string addr, int port) : Client(addr, port)
    {
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
            throw ERROR::SOCKET_CREATE;

        if (connect(sock, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) 
            throw ERROR::CONNECT;
    } 

    
    static std::shared_ptr<TcpClient> new_(std::string addr, int port) {
        std::shared_ptr<TcpClient> new_ = std::make_shared<TcpClient>(addr, port);//(new ex);
        return new_;
    }
    void handleConnect()override
    {
        struct timeval tv;
        tv.tv_sec = 5;
        tv.tv_usec = 0;
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
        bool stop(false);
        std::string s;
        while (!isStop(s = getMsg()))
        {
            memset(buffer, 0, sizeof(buffer)); 

            int len = s.length();
            send(sock, &len, sizeof(int), MSG_NOSIGNAL);
            send(sock, s.c_str(), len, MSG_NOSIGNAL);
            //write(sock, s.c_str(), sizeof(s.size())); 
            recv(sock, &len, sizeof(int), MSG_NOSIGNAL);
            recv(sock, buffer, len, MSG_NOSIGNAL); 
            std::cout <<"server : " << std::string(buffer,len) << std::endl; 
        }
        std::cout <<"stop\n";
    }
    ~TcpClient()
    {
        //std::cout << "~TcpClient"<<std::endl;
        std::string s("stop");
        int len = s.length();
        write(sock, &len, sizeof(int));
        write(sock, s.c_str(), len);
        if (sock >= 0) close(sock);
    }
};

class UdpClient : public Client
{
   
public:
    UdpClient(std::string addr, int port) : Client(addr, port)
    {
        std::cout << "constr UDP" << std::endl;
        
        if ( (sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) 
            throw ERROR::SOCKET_CREATE;
    }
    
    ~UdpClient()
    {
        if (sock >= 0) close(sock);
    }
    void handleConnect()override
    {
        struct timeval tv;
        tv.tv_sec = 5;
        tv.tv_usec = 0;
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

        bool stop(false);
        std::string s;
        while (!isStop(s = getMsg()))
        {
            unsigned int len;
            int n = 0;
            sendto(sock, s.c_str(), s.size(), 
            MSG_CONFIRM, (const struct sockaddr *) &servaddr,  
            sizeof(servaddr)); 
            memset(buffer, 0, sizeof(buffer)); 
            n = recvfrom(sock, (char *)buffer, config::MAX_SIZE_MSG,  
                MSG_WAITALL, ( struct sockaddr *) &servaddr, 
                &len); 
            buffer[n] = '\0'; 
            
            if (n < 0) std::cout << "Server is not responding"<< std::endl;
            else std::cout <<"server : " << std::string(buffer) << std::endl;
        }
    }
    
};

int main(int argc, char ** argv) 
{ 
    try
    {
        if (argc != 4) throw Client::ERROR::AMOUNT_OF_ARGUMENTS;
         std::string addr(argv[1]);
         int port = atoi(argv[2]);
         std::string tProtocol(argv[3]);
         std::cout << addr << port << tProtocol << std::endl;
         std::shared_ptr<Client> cl;
         
         if ("udp" == tProtocol)
         {
            //cl = UdpClient::new_(addr, port);
            cl = std::make_shared<UdpClient>(addr, port);
         } 
         else if ("tcp" == tProtocol)
         {
            cl = std::make_shared<TcpClient>(addr, port);
         }
         else throw Client::ERROR::TYPE_PROTOCOL;
         
         cl->handleConnect();
    }
    catch(Client::ERROR er)
    {
        Client::printEr(er);
    }

} 