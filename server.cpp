#include <memory>
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <cstring> 
#include "config.h"


typedef struct
{
	int sock;
	struct sockaddr address;
	unsigned int addr_len;
} connection_t;

class TCP_Server
{	

public:
	static int sock;
	static struct sockaddr_in address;
	static int port;	
	char buffer[config::MAX_SIZE_MSG];
	connection_t conn;

	static int init()
	{
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sock <= 0) { return -3; }
		
		address.sin_family = AF_INET;
		address.sin_addr.s_addr = INADDR_ANY;
		address.sin_port = htons(port);

		if (bind(sock, (struct sockaddr *)&address, sizeof(struct sockaddr_in)) < 0) { return -4; }
		if (listen(sock, 5) < 0) { return -5;}
	}
	
	TCP_Server(){std::cout << "tcp ctr"<<std::endl;}
	
	static std::shared_ptr<TCP_Server> new_() {
        std::shared_ptr<TCP_Server> new_ = std::make_shared<TCP_Server>();
        return new_;
    }
    void proccess()
    {
    	bool stop(false);
    	struct timeval tv;
	    tv.tv_sec = 60;
	    tv.tv_usec = 0;
	    setsockopt(conn.sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    	auto start = std::chrono::system_clock::now();
    	while (!stop)
    	{
		    if ((std::chrono::system_clock::now() - start) < (std::chrono::duration<double>)59)
		    {
		    	start = std::chrono::system_clock::now();

			    memset(buffer, 0, sizeof(buffer)); 

				int len(0);
			    recv(conn.sock, &len, sizeof(int), MSG_NOSIGNAL);
			    recv(conn.sock, buffer, len, MSG_NOSIGNAL);
			    std::string msg(buffer, len);
			    //std::cout << msg<<std::endl;
			    if ("stop"  == msg) stop = true;
			    else
			    {
		    	send(conn.sock, &len, sizeof(int), MSG_NOSIGNAL);
		    	send(conn.sock, (const char*)buffer, len, MSG_NOSIGNAL);
				}
			}
			else stop = true;
		}
    }


	~TCP_Server()
	{ 
		std::cout << "tcp destr"<<std::endl;
		if (conn.sock >= 0)
		{
			shutdown(conn.sock, SHUT_RDWR);
			close(conn.sock); 
		}
	}
	static void handle()
	{
		init();
		std::cout << "TCP listen" << std::endl;
		while (true)
		{
			std::shared_ptr<TCP_Server> client = TCP_Server::new_();
			std::string s ;

			client->conn.sock = accept(TCP_Server::sock, &client->conn.address, &client->conn.addr_len);
			if (client->conn.sock <= 0)
			{
				//printf("fail");
			}
			else
			{
				auto th = std::thread(&TCP_Server::proccess, client);
				th.detach();
			}
		}
	}
};

class UDP_Server
{

public:
	char buffer[config::MAX_SIZE_MSG];
	static std::shared_ptr<UDP_Server> new_() {
        std::shared_ptr<UDP_Server> new_ = std::make_shared<UDP_Server>();
        return new_;
    }
	static int sock;
	static struct sockaddr_in address;
	static int port;

	static void init()
	{
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);
	
    sock = socket(AF_INET, SOCK_DGRAM, 0); 
    
    bind(sock, (struct sockaddr*)&address, sizeof(struct sockaddr_in));
	}
	UDP_Server(){};
	~UDP_Server(){};
	int n;
	unsigned int len;
	struct sockaddr_in address_c;
	void proccess (void)
	{
    sendto(sock, (const char *)buffer, n,  
        MSG_CONFIRM, (const struct sockaddr *) &address_c, 
            len); 
	}
	static void handle()
	{
		init();
		std::cout << "UDP listen" << std::endl;
		while (true)
		{
			std::shared_ptr<UDP_Server> client = UDP_Server::new_();
			
			client->len = sizeof(struct sockaddr_in); 
		  
		    client->n = recvfrom(UDP_Server::sock, (char *)(client->buffer), 10,  
		                MSG_WAITALL, ( struct sockaddr *) &(client->address_c), 
		                &(client->len)); 
		    
		    if (client->n > 0)
			{
				auto th = std::thread(&UDP_Server::proccess, client);
				th.detach();
			}
		}
	}
};


int TCP_Server::sock = -1;
int TCP_Server::port = config::PORT;
sockaddr_in TCP_Server::address;

int UDP_Server::sock = -1;
struct sockaddr_in UDP_Server::address;
int UDP_Server::port = config::PORT;



int main()
{
	std::thread tTCP(TCP_Server::handle);
	std::thread tUDP(UDP_Server::handle);
	tTCP.join();
	tUDP.join();

	return 0;
}